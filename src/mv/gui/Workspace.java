/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;

import com.sun.javafx.geom.Rectangle;
import java.util.ArrayList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import saf.components.AppWorkspaceComponent;
import mv.MapViewerApp;
import saf.controller.AppFileController;
import static saf.settings.AppPropertyType.EXIT_ICON;
import static saf.settings.AppPropertyType.EXIT_TOOLTIP;
import static saf.settings.AppPropertyType.LOAD_ICON;
import static saf.settings.AppPropertyType.LOAD_TOOLTIP;
import static saf.settings.AppPropertyType.NEW_ICON;
import static saf.settings.AppPropertyType.NEW_TOOLTIP;

/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {
    MapViewerApp app;
    AppFileController fctr;
    Pane workspace;
    
    private Button loadButton;
    private Button exitButton;
    private FlowPane toolbar;
    
    private Line[] latitude;
    private Line[] longitude;
    boolean gridOn;
    
    private int magX;
    private int magY;
    
    public Workspace(MapViewerApp initApp) {
        app = initApp;
        workspace = new Pane();
        gridOn = false;
        
        magX = 1;
        magY = 1;
        
        alterToolbar();
        setUpGUI();
        establishHandlers();
        
        
    }
    
    private void alterToolbar() {
        toolbar = new FlowPane();
        loadButton = app.getGUI().initChildButton(toolbar,LOAD_ICON.toString(),LOAD_TOOLTIP.toString(),false);
        exitButton = app.getGUI().initChildButton(toolbar,EXIT_ICON.toString(),EXIT_TOOLTIP.toString(),false);
        
        fctr = new AppFileController(app);
        loadButton.setOnAction(e -> {
            fctr.handleLoadRequest();
        });
        exitButton.setOnAction(e -> {
            fctr.handleExitRequest();
        });
        app.getGUI().getAppPane().setTop(toolbar);
        toolbar.getStyleClass().add(CLASS_BORDERED_PANE);
    }
    
    private void setUpGUI() {
        workspace.setStyle("-fx-background-color: blue;");
        app.getGUI().getAppPane().setCenter(workspace);
        app.getGUI().getWindow().setFullScreen(true);
    }
    
    private void setUpLines() {
        gridOn = false;
        latitude = new Line[5];
        longitude= new Line[11];
        
        int i = 0;
        double deg = 30.0;
        
        for (; deg <= 150; deg+=30.0, i++) {
            latitude[i] = new Line(0.0, deg * yRatio(), 360 * xRatio(), deg * yRatio());
            latitude[i].setStroke(Color.WHITE);
            latitude[i].setOpacity(0.0);
            if (deg != 90.0) latitude[i].getStrokeDashArray().addAll(1d, 4d);
        }
        i = 0;
        deg = 30.0;
        for (; deg <= 330; deg+=30.0, i++) {
            longitude[i] = new Line(deg * xRatio(), 0.0, deg * xRatio(), 180 * yRatio());
            longitude[i].setStroke(Color.WHITE);
            longitude[i].setOpacity(0.0);
            if ((int)deg % 180 != 0) longitude[i].getStrokeDashArray().addAll(1d, 4d);
        }
    }
    
    private void establishHandlers() {
        Scene z = app.getGUI().getWindow().getScene();
        workspace.setOnMouseClicked(e-> {
            System.out.println("COORDS:");
            System.out.println(e.getX());
            System.out.println(e.getY());
            
            if (e.getButton() == MouseButton.SECONDARY) { // ZOOM OUT
                workspace.setScaleX(workspace.getScaleX()/2.0);
                workspace.setScaleY(workspace.getScaleY()/2.0);
            }
            else if (e.getButton() == MouseButton.PRIMARY) { // ZOOM IN ON CLICK
                //double shiftX = (workspace.getWidth() * workspace.getScaleX()) / 2;
                //double shiftY = (workspace.getHeight() * workspace.getScaleY()) / 2;
                
                //Circle anchor = new Circle();
                //anchor.setCenterX(e.getX());
                //anchor.setCenterY(e.getY());
                //anchor.setRadius(1);
                //workspace.getChildren().add(anchor);
                
                workspace.setScaleX(workspace.getScaleX() * 2.0);
                workspace.setScaleY(workspace.getScaleY() * 2.0);
                
                //workspace.setTranslateX(shiftX * magX);
                //workspace.setTranslateY(shiftY * magY);
                
                //magX *= 2;
                //magY *= 2;
                
                //workspace.setTranslateX(workspace.getTranslateX() - (e.getX() * 2.0));
                //workspace.setTranslateY(workspace.getTranslateY() - (e.getY() * 2.0));
                //*/
            }
        });
        
        z.setOnKeyTyped(e -> {
           if (e.getCharacter().equals("w")) { // CAMERA UP
               workspace.setTranslateY(workspace.getTranslateY() + 20);
           }
           if (e.getCharacter().equals("s")) { // CAMERA DOWN
               workspace.setTranslateY(workspace.getTranslateY() - 20);
           }
           if (e.getCharacter().equals("a")) { // CAMERA LEFT
               workspace.setTranslateX(workspace.getTranslateX() + 20);
           }
           if (e.getCharacter().equals("d")) { // CAMERA RIGHT
               workspace.setTranslateX(workspace.getTranslateX() - 20);
           }
           if (e.getCharacter().equals("f")) { // FULLSCREEN TOGGLE
               if (app.getGUI().getWindow().isFullScreen()) app.getGUI().getWindow().setFullScreen(false);
               else app.getGUI().getWindow().setFullScreen(true);
           }
           if (e.getCharacter().equals("g")) { // GRIDLINES TOGGLE
               if (gridOn) {
                    for (int i = 0; i < latitude.length; i++) latitude[i].setOpacity(0.0);
                    for (int i = 0; i < longitude.length; i++) longitude[i].setOpacity(0.0);
                    gridOn = false;
               } else {
                    for (int i = 0; i < latitude.length; i++) latitude[i].setOpacity(100.0);
                    for (int i = 0; i < longitude.length; i++) longitude[i].setOpacity(100.0);
                    gridOn = true;
               }
           }
           if (e.getCharacter().equals("r")) { // RESET
               workspace.setScaleX(1);
               workspace.setScaleY(1);
               workspace.setTranslateX(0);
               workspace.setTranslateY(0);
           }
        });
    }

    @Override
    public void reloadWorkspace() {
        
    }

    @Override
    public void initStyle() {
        
    }
    
    public void loadIntoMap(ArrayList<Polygon> a) {
        workspace.setTranslateX(0);
        workspace.setTranslateY(0);
        workspace.setScaleX(1);
        workspace.setScaleY(1);
        workspace.getChildren().clear();
        workspace.getChildren().addAll(a);
        
        setUpLines();
        workspace.getChildren().addAll(latitude);
        workspace.getChildren().addAll(longitude);
    }
    
    public double yRatio() {
        return workspace.getHeight() / 180.0;
    }
    public double xRatio() {
        return workspace.getWidth() / 360.0;
    }
    
    public double xConvInv(double x) {
        return x - 180;
    }
    public double yConvInv(double y) {
        return 90 - y;
    }
    
    public void activateWorkspace(BorderPane appPane) {
        if (!workspaceActivated) {
            // PUT THE WORKSPACE IN THE GUI
            appPane.setCenter(workspace);
            workspaceActivated = true;
        }
    }
    
    private double xConv(double a) {
        return a + 180;
    }
    private double yConv(double b) {
        return 0 - (b - 90);
    }
}