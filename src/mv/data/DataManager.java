package mv.data;

import java.util.ArrayList;
import javafx.scene.shape.Polygon;
import saf.components.AppDataComponent;
import mv.MapViewerApp;
import mv.gui.Workspace;

/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    MapViewerApp app;
    ArrayList<Polygon> polygons;
    
    public DataManager(MapViewerApp initApp) {
        app = initApp;
    }
    
    @Override
    public void reset() {
        
    }
    
    public void loadData(ArrayList<Polygon> a) {
        polygons = a;
        Workspace b = (Workspace)app.getWorkspaceComponent();
        b.loadIntoMap(polygons);
    }
    
    public double xRatio () {
        Workspace w = (Workspace)app.getWorkspaceComponent();
        return w.xRatio();
    }
    public double yRatio() {
        Workspace w = (Workspace)app.getWorkspaceComponent();
        return w.yRatio();
    }
}
