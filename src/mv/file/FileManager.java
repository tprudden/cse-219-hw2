/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import mv.data.DataManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dm = (DataManager)data;
        
        JsonObject jFile = loadJSONFile(filePath);
        
        ArrayList<Polygon> polygons = new ArrayList<Polygon>();
        
        JsonArray subregions = jFile.getJsonArray("SUBREGIONS");
        for (int i = 0; i < subregions.size(); i++) {
            JsonObject currentSR = subregions.getJsonObject(i);
            //Polygon subPoly = new Polygon();
            //subPoly.setFill(Color.GREENYELLOW);
            JsonArray subPolygonSet = currentSR.getJsonArray("SUBREGION_POLYGONS");
            for (int j = 0; j < subPolygonSet.size(); j++) {
                JsonArray subPolygonCoords = subPolygonSet.getJsonArray(j);
                //System.out.println(subPolygonCoords.size());
                Polygon subPoly = new Polygon();
                subPoly.setFill(Color.GREENYELLOW);
                subPoly.setStroke(Color.BLACK);
                for (int k = 0; k < subPolygonCoords.size(); k++) {
                    JsonObject vertex = subPolygonCoords.getJsonObject(k);
                    JsonNumber x = vertex.getJsonNumber("X");
                    JsonNumber y = vertex.getJsonNumber("Y");
                    subPoly.getPoints().add(xConv(x.doubleValue()) * dm.xRatio());
                    subPoly.getPoints().add(yConv(y.doubleValue()) * dm.yRatio());
                    
                    //System.out.println("X: " + x.doubleValue());
                    //System.out.println("Y: " + y.doubleValue());
                }
                //System.out.println("-------------------");
                polygons.add(subPoly);
            }
            
        }
        dm.loadData(polygons);
        System.out.print(subregions.size());
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private double xConv(double a) {
        return a + 180;
    }
    private double yConv(double b) {
        return 0 - (b - 90);
    }


}
